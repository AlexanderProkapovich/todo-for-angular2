import { Component, Output, EventEmitter } from "angular2/core"

@Component({
  selector: 'TodoForm',
  template: `
  <div class="container">
    <div class="col-xs-12">
      <form>
        <div class="form-group">
          <input type="text" (keyup.enter)="onEnter(item)" #item autofocus class="form-control">
        </div>
      </form>
    </div>
  </div>
  `
})
export class TodoForm {
  @Output() addTodo:EventEmitter<any> = new EventEmitter();

  onEnter(item) {
    this.addTodo.emit(item.value);
    item.value = "";
  }
}
