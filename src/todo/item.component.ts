import { Component, Input, Output, EventEmitter } from "angular2/core"
import { ITodo } from "./service";

@Component({
  selector: 'TodoItem',
  template: `
    <li [class.completed]="todo.completed">
      <div class="col-xs-12">
        <div class="input-group">
          <span class="input-group-addon">
            <input type="checkbox" [ngModel]="todo.completed" (click)="toggleTodo.emit(todo)" />
          </span>
          <div class="form-control">
            {{todo.title}}
          </div>
          <span class="input-group-btn">
            <button class="btn btn-default" (click)="removeTodo.emit(todo)">Remove</button>
          </span>
        </div>
      </div>
    </li>
  `
})
export class TodoItem {
  @Input() todo:ITodo;
  @Output() removeTodo:EventEmitter<any> = new EventEmitter();
  @Output() toggleTodo:EventEmitter<any> = new EventEmitter();
}
